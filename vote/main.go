package main

import (
	"database/sql"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	_ "github.com/joho/godotenv/autoload"
	_ "github.com/lib/pq"
)

var db *sql.DB
var guildID string = "533012173497303060"
var roleID string = "1050902323188219994"

var sig chan os.Signal

func init() {
	sig = make(chan os.Signal, 1)
}

func respond(s *discordgo.Session, e *discordgo.InteractionCreate, content string) error {
	return s.InteractionRespond(e.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: content,
			Flags:   1 << 6,
		},
	})
}

func interactionHandler(session *discordgo.Session, event *discordgo.InteractionCreate) {
	if event.ApplicationCommandData().Name == "vote" {
		botID := event.ApplicationCommandData().Options[0].Value

		_, err := db.Exec("INSERT INTO votes VALUES ($1, $2, $3);", event.Member.User.ID, time.Now(), botID)
		if err != nil {
			log.Println(event.Member.User.String(), err)
			respond(session, event, "Failed to insert record")
		} else {
			err := session.GuildMemberRoleAdd(event.GuildID, event.Member.User.ID, roleID)
			if err != nil {
				log.Println(event.Member.User.String(), err)
			}
			respond(session, event, "thanks for your vote")
		}

	}
}

func wipeRoles(session *discordgo.Session, ready *discordgo.Ready) {
	log.Println(session.State.User.String() + " wiping...")

	session.Guild(guildID)
	guild, _ := session.State.Guild(guildID)

	for _, member := range guild.Members {
		for _, role := range member.Roles {
			if roleID == role {
				err := session.GuildMemberRoleRemove(guildID, member.User.ID, role)
				if err != nil {
					log.Println(member.User.String(), err)
				}
				log.Println("Wiped " + member.User.String())
				break
			}
		}
	}

	sig <- os.Interrupt
}

func main() {
	session, err := discordgo.New("Bot " + os.Getenv("TOKEN"))
	if err != nil {
		log.Fatalln(err)
	}

	session.Identify.Intents = discordgo.IntentGuilds | discordgo.IntentGuildMembers | discordgo.IntentGuildPresences

	if len(os.Args) > 1 {
		session.AddHandler(wipeRoles)
	} else {
		db, _ = sql.Open("postgres", "postgres://postgres:password@postgres/postgres?sslmode=disable")

		if err := db.Ping(); err != nil {
			log.Fatalln(err)
		}

		session.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) { log.Println(s.State.User.String() + " ready") })
		session.AddHandler(interactionHandler)
	}

	err = session.Open()
	if err != nil {
		log.Fatalln(err)
	}
	defer session.Close()

	// log.Println(session.ApplicationCommandCreate(session.State.User.ID, "533012173497303060", voteCommand))

	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	<-sig
}
