package main

import "github.com/bwmarrin/discordgo"

var voteCommand *discordgo.ApplicationCommand = &discordgo.ApplicationCommand{
	Name:        "vote",
	Description: "Submit your daily vote best bot performance",
	Options: []*discordgo.ApplicationCommandOption{
		{
			Type:        discordgo.ApplicationCommandOptionString,
			Name:        "vote",
			Description: "The best bot",
			Required:    true,
			Choices: []*discordgo.ApplicationCommandOptionChoice{
				{
					Name:  "Smol Botter",
					Value: "271367658916544514",
				},
				{
					Name:  "Yaayoi",
					Value: "360834554212253697",
				},
				{
					Name:  "Santa",
					Value: "1049495735533568010",
				},
				{
					Name:  "Sicko",
					Value: "1049351919488999566",
				},
				{
					Name:  "@Snus_masta",
					Value: "1049349867585163294",
				},
				{
					Name:  "Zuiikin Gals",
					Value: "1049346298840612864",
				},
				{
					Name:  "Nathan",
					Value: "1049348387255885974",
				},
				{
					Name:  "Shroomjak",
					Value: "1049351346429632622",
				},
			},
		},
	},
}
