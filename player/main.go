package main

import (
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	_ "github.com/joho/godotenv/autoload"
	"layeh.com/gopus"
)

const (
	channels  int = 2                   // 1 for mono, 2 for stereo
	frameRate int = 48000               // audio sampling rate
	frameSize int = 960                 // uint16 size of each audio frame
	maxBytes  int = (frameSize * 2) * 2 // max size of opus data
)

var voice *discordgo.VoiceConnection

var tracklist []string

func init() {
	files, err := ioutil.ReadDir("music/")
	if err != nil {
		log.Fatalln(err)
	}

	for _, file := range files {
		tracklist = append(tracklist, file.Name())
	}

	rand.Seed(0x542)
	rand.Shuffle(len(tracklist), func(i, j int) { tracklist[i], tracklist[j] = tracklist[j], tracklist[i] })
}

func currentTrack() string {
	index := ((time.Now().Day() % 16) - 9 + 16) % 16

	if index == 9 {
		return "carey.m4a"
	} else if index > 9 {
		index--
	}

	// log.Println("Playing: ", tracklist[index])
	return "music/" + tracklist[index]
}

func onReady(session *discordgo.Session, ready *discordgo.Ready) {
	log.Println(ready.User.String() + " ready")

	var err error
	voice, err = session.ChannelVoiceJoin("533012173497303060", os.Getenv("CHANNEL"), false, true) // Boy Club, Jans Jail
	if err != nil {
		log.Println(err)
	}

	opusEncoder, err := gopus.NewEncoder(frameRate, channels, gopus.Audio)
	if err != nil {
		log.Println(err)
	}

	for {
		pcm, _ := openStream(currentTrack())
		func() {
			for {
				select {
				case recv, ok := <-pcm:
					if !ok {
						return
					}

					opus, err := opusEncoder.Encode(recv, frameSize, maxBytes)
					if err != nil {
						log.Println(err)
					}

					voice.OpusSend <- opus
				}
			}
		}()
	}
}

func main() {
	session, err := discordgo.New("Bot " + os.Getenv("TOKEN"))
	if err != nil {
		log.Fatalln(err)
	}

	//session.Identify.Presence.Status = string(discordgo.StatusInvisible)
	session.Identify.Intents = discordgo.IntentGuilds | discordgo.IntentsGuildMessages | discordgo.IntentGuildVoiceStates

	session.AddHandler(onReady)
	session.AddHandler(func(session *discordgo.Session, event *discordgo.VoiceStateUpdate) {

		if !event.Member.User.Bot {
			if event.BeforeUpdate != nil && event.BeforeUpdate.ChannelID == os.Getenv("CHANNEL") {

				if time.Now().Day() == 24 {
					return
				}

				guild, _ := session.State.Guild(event.GuildID)
				for _, state := range guild.VoiceStates {
					if state.ChannelID == event.BeforeUpdate.ChannelID {
						if !state.Member.User.Bot {
							return
						}
					}
				}
				voice.Disconnect()
				log.Fatalln("merry christmas")

			}
		}
	})

	err = session.Open()
	if err != nil {
		log.Fatalln(err)
	}
	defer session.Close()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	<-sig
}
