module player

go 1.19

require (
	github.com/bwmarrin/discordgo v0.26.1
	github.com/joho/godotenv v1.4.0
	layeh.com/gopus v0.0.0-20210501142526-1ee02d434e32
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	golang.org/x/crypto v0.3.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
)
