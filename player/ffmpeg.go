package main

import (
	"bufio"
	"encoding/binary"
	"io"
	"log"
	"os"
	"os/exec"
)

func openStream(filename string) (chan []int16, *os.Process) {
	out := make(chan []int16, 2)

	ffmpeg := exec.Command(
		"/usr/bin/ffmpeg", "-i", filename,
		"-af", "loudnorm", "-f", "s16le",
		"-ar", "48000", "-ac", "2", "pipe:1")

	ffmpegout, err := ffmpeg.StdoutPipe()
	if err != nil {
		close(out)
	}

	ffmpegbuff := bufio.NewReaderSize(ffmpegout, 16384)

	err = ffmpeg.Start()
	if err != nil {
		log.Fatalln(err)
		close(out)
	}

	go func() {
		defer close(out)

		for {
			audiobuf := make([]int16, frameSize*2)
			err = binary.Read(ffmpegbuff, binary.LittleEndian, &audiobuf)
			if err == io.EOF || err == io.ErrUnexpectedEOF {
				break
			}
			if err != nil {
				break
			}

			out <- audiobuf
		}

		if ffmpeg.Process != nil {
			ffmpeg.Process.Kill()
		}

	}()

	return out, ffmpeg.Process
}
